@setlocal
@set ZIPFILE=src.zip
@set TARGETS=-C src .
@if exist %ZIPFILE% ( goto updatezip )

jar cvfM %ZIPFILE% %TARGETS%

@endlocal
@exit /B

:updatezip

jar uvfM %ZIPFILE% %TARGETS%

@endlocal
