import java.io.*;
import java.util.*;

class PQwithTpIIa extends PriorityQueue<TpIIa> {}
class PQwithTpIIaD extends PriorityQueue<TpIIaD> {}

public class PolyominoCovering {

    static final Random rand = new Random(1983_1983_1983L);

    int[] grid, tiles;
    int N, T;

    public int[] findSolution(int N, int[] grid, int[] tiles) {
        long limit = System.currentTimeMillis() + 8500L;
        this.N = N;
        this.T = tiles.length;
        this.grid = grid;
        this.tiles = tiles;

        int[] nums = new int[20];
        for (int g : grid) {
            nums[g + 9]++;
        }
        System.err.println("N: " + N);
        System.err.println("p789: " + nums[16]+","+nums[17]+","+nums[18]);
        System.err.println("p456: " + nums[13]+","+nums[14]+","+nums[15]);
        System.err.println("p123: " + nums[10]+","+nums[11]+","+nums[12]);
        System.err.println("zero: " + nums[9]);
        System.err.println("n123: " + nums[6]+","+nums[7]+","+nums[8]);
        System.err.println("n456: " + nums[3]+","+nums[4]+","+nums[5]);
        System.err.println("n789: " + nums[0]+","+nums[1]+","+nums[2]);
        System.err.println("t234: " + tiles[0]+","+tiles[1]+","+tiles[2]);
        System.err.println("t567: " + tiles[3]+","+tiles[4]+","+tiles[5]);

        int[] ret = new int[N * N];
        Arrays.fill(ret, -1);

        long best = -1000L * (long)(N * N);

        {
            int[] tmp = new int[N * N];
            Arrays.fill(tmp, -1);
            solveBadGreedy(tmp, initGreedy(), new boolean[N * N]);
            long score = poorHillClimb(tmp, 10L);
            if (score > best) {
                best = score;
                ret = tmp;
            }
        }

        {
            int[] tmp = new int[N * N];
            Arrays.fill(tmp, -1);
            solveGreedyWithAvg(tmp, initGreedyWithAvg(), new boolean[N * N]);
            long score = poorHillClimb(tmp, 10L);
            if (score > best) {
                best = score;
                ret = tmp;
            }
        }

        {
            TpIIaD.sign = -1;
            int[] tmp = new int[N * N];
            Arrays.fill(tmp, -1);
            solveGreedyWithAvg(tmp, initGreedyWithAvg(), new boolean[N * N]);
            long score = poorHillClimb(tmp, 10L);
            if (score > best) {
                best = score;
                ret = tmp;
            }
        }

        PQwithTpIIa[] pqs = initGreedy();
        for (int t = 7; t >= 2; t--) {
            if (tiles[t - 2] == 0) { continue; }
            PQwithTpIIa pq = pqs[t];
            while (!pq.isEmpty()) {
                if (System.currentTimeMillis() > limit) {
                    break;
                }
                TpIIa tp = pq.poll();
                int[] tmp = new int[N * N];
                boolean[] visited = new boolean[N * N];
                Arrays.fill(tmp, -1);
                int[] remains = Arrays.copyOf(tiles, tiles.length);
                for (int p : tp.item2) {
                    visited[p] = true;
                    tmp[p] = 1;
                }
                remains[t - 2]--;
                solveGreedy(tmp, initGreedy(), visited, remains, 2);
                long score = poorHillClimb(tmp, 10L);
                if (score > best) {
                    best = score;
                    ret = tmp;
                }
            }
        }
        System.err.println("sc: " + best);
        reId(ret);

        best = poorSA(ret, best, limit - System.currentTimeMillis() + 1000L);
        System.err.println("sc: " + best);

        return ret;
    }

    void solveFoo(int[] ret) {
        int sizeLimit = 300;

        State best = null;
        TpIIa[] list = initFoo();
        PriorityQueue<State> pq = new PriorityQueue<>();
        for (int i = 0; i < list.length; i++) {
            TpIIa tp = list[i];
            if (tiles[tp.item2.length - 2] == 0) { continue; }
            BitFlag bf = new BitFlag(N * N);
            bf.setAll(tp.item2);
            State st = new State(-1000L * (long)(N * N - tp.item2.length) + (long)tp.item1, 0, Arrays.copyOf(tiles, tiles.length), bf, new Node<>(null, tp.item2));
            st.remains[tp.item2.length - 2]--;
            pq.add(st);
            if (best == null || st.compareTo(best) >= 0) {
                best = st;
            }
            if (pq.size() == sizeLimit) {
                break;
            }
        }

        int indexIncl = 5;
        double accRate = 0.01;

        while (!pq.isEmpty()) {
            PriorityQueue<State> tmp = new PriorityQueue<>();
            for (State st : pq) {
                if (st.compareTo(best) > 0) {
                    best = st;
                }
                if (st.index >= list.length) {
                    continue;
                }
                for (int i = 0; i < indexIncl; i++) {
                    int index = st.index + i;
                    if (index >= list.length) {
                        break;
                    }
                    TpIIa tp = list[index];
                    if (st.remains[tp.item2.length - 2] == 0) {
                        continue;
                    }
                    if (st.flag.exists(tp.item2[0])) {
                        continue;
                    }
                    if (st.flag.any(tp.item2)) {
                        int t = tp.item2.length;
                        int p = tp.item2[0];
                        int[] cur = new int[t];
                        int[] tmpBest = new int[t];
                        int[] bestScore = new int[]{ -1000 * t };
                        int row = p / N;
                        int col = p % N;
                        cur[0] = p;
                        st.flag.set(p);
                        dfs(st.flag, 1, row, col, grid[p], cur, bestScore, tmpBest);
                        st.flag.remove(p);
                        if (bestScore[0] > -1000 * t) {
                            long score = st.score + (long)bestScore[0] + 1000L * (long)tmpBest.length;
                            if (tmp.size() < sizeLimit) {
                                int[] rem = Arrays.copyOf(st.remains, st.remains.length);
                                rem[tmpBest.length - 2]--;
                                BitFlag bf = st.flag.getCopy();
                                bf.setAll(tmpBest);
                                tmp.add(new State(score, st.index + 1, rem, bf, st.node.append(tmpBest)));
                            } else if (score > tmp.peek().score
                                    || (score == tmp.peek().score && rand.nextDouble() < accRate)) {
                                State re = tmp.poll();
                                re.score = score;
                                re.index = st.index + 1;
                                System.arraycopy(st.remains, 0, re.remains, 0, re.remains.length);
                                re.remains[tmpBest.length - 2]--;
                                st.flag.copyTo(re.flag);
                                re.flag.setAll(tmpBest);
                                re.node = st.node.append(tmpBest);
                                tmp.add(re);
                            }
                        }
                        continue;
                    }
                    long score = st.score + (long)tp.item1 + 1000L * (long)tp.item2.length;
                    if (tmp.size() < sizeLimit) {
                        int[] rem = Arrays.copyOf(st.remains, st.remains.length);
                        rem[tp.item2.length - 2]--;
                        BitFlag bf = st.flag.getCopy();
                        bf.setAll(tp.item2);
                        tmp.add(new State(score, st.index + 1, rem, bf, st.node.append(tp.item2)));
                    } else if (score > tmp.peek().score
                            || (score == tmp.peek().score && rand.nextDouble() < accRate)) {
                        State re = tmp.poll();
                        re.score = score;
                        re.index = st.index + 1;
                        System.arraycopy(st.remains, 0, re.remains, 0, re.remains.length);
                        re.remains[tp.item2.length - 2]--;
                        st.flag.copyTo(re.flag);
                        re.flag.setAll(tp.item2);
                        re.node = st.node.append(tp.item2);
                        tmp.add(re);
                    }
                }
                st.index += indexIncl;
                if (tmp.size() < sizeLimit) {
                    tmp.add(st);
                } else if (st.compareTo(tmp.peek()) >= 0) {
                    tmp.poll();
                    tmp.add(st);
                }
            }
            pq = tmp;
        }

        int id = 1;
        Node<int[]> node = best.node;
        while (node != null) {
            for (int p : node.item) {
                ret[p] = id;
            }
            id++;
            node = node.prev;
        }

    }

    TpIIa[] initFoo() {
        boolean[] visited = new boolean[N * N];
        List<TpIIa> list = new ArrayList<>(N * N * T);
        int[] cur = new int[7];
        int[] bestScores = new int[8];
        int[][] bests = new int[8][];
        for (int row = 0; row < N; row++) {
            for (int col = 0; col < N; col++) {
                for (int i = 2; i < bestScores.length; i++) {
                    bestScores[i] = -1000 * i;
                    bests[i] = new int[i];
                }
                int p = row * N + col;
                cur[0] = p;
                visited[p] = true;
                dfs(visited, 1, row, col, grid[p], cur, bestScores, bests);
                visited[p] = false;
                for (int i = 2; i < bestScores.length; i++) {
                    if (bestScores[i] == -1000 * i) { continue; }
                    list.add(new TpIIa(bestScores[i], bests[i]));
                }
            }
        }
        Collections.sort(list);
        return list.toArray(new TpIIa[0]);
    }

    PQwithTpIIa[] initGreedy() {
        boolean[] visited = new boolean[N * N];
        PQwithTpIIa[] pqs = new PQwithTpIIa[8];
        {
            for (int i = 2; i < pqs.length; i++) {
                pqs[i] = new PQwithTpIIa();
            }
            int[] cur = new int[7];
            int[] bestScores = new int[8];
            int[][] bests = new int[8][];
            for (int row = 0; row < N; row++) {
                for (int col = 0; col < N; col++) {
                    for (int i = 2; i < bestScores.length; i++) {
                        bestScores[i] = -1000 * i;
                        bests[i] = new int[i];
                    }
                    int p = row * N + col;
                    cur[0] = p;
                    visited[p] = true;
                    dfs(visited, 1, row, col, grid[p], cur, bestScores, bests);
                    visited[p] = false;
                    for (int i = 2; i < bestScores.length; i++) {
                        if (bestScores[i] == -1000 * i) { continue; }
                        pqs[i].add(new TpIIa(bestScores[i], bests[i]));
                    }
                }
            }
        }
        return pqs;
    }

    PQwithTpIIaD[] initGreedyWithAvg() {
        boolean[] visited = new boolean[N * N];
        PQwithTpIIaD[] pqs = new PQwithTpIIaD[8];
        {
            for (int i = 2; i < pqs.length; i++) {
                pqs[i] = new PQwithTpIIaD();
            }
            int[] cur = new int[7];
            long[][] bestScores = new long[8][3];
            int[][] bests = new int[8][];
            for (int row = 0; row < N; row++) {
                for (int col = 0; col < N; col++) {
                    for (int i = 2; i < bestScores.length; i++) {
                        bestScores[i][0] = -1000L * (long)i;
                        bests[i] = new int[i];
                    }
                    int p = row * N + col;
                    cur[0] = p;
                    visited[p] = true;
                    dfsWithSum(visited, 1, row, col, (long)grid[p], cur, bestScores, bests);
                    visited[p] = false;
                    for (int i = 2; i < bestScores.length; i++) {
                        if (bestScores[i][0] == -1000L * (long)i) { continue; }
                        pqs[i].add(new TpIIaD((int)bestScores[i][0], bests[i],
                            (double)bestScores[i][1] / (double)bestScores[i][2]));
                    }
                }
            }
        }
        return pqs;
    }

    void solveGreedy(int[] ret, PQwithTpIIa[] pqs, boolean[] visited, int[] remains, int id) {
    outerloop:
        for (int t = 7; t >= 2; t --) {
            PQwithTpIIa pq = pqs[t];
            for (int i = remains[t - 2]; i > 0; i--) {
                while (!pq.isEmpty()) {
                    TpIIa tp = pq.poll();
                    int[] list = tp.item2;
                    if (visited[list[0]]) { continue; }
                    if (tp.item1 <= 0) {
                        pq.add(tp);
                        continue outerloop;
                    }
                    boolean ok = true;
                    for (int p : list) {
                        if (visited[p]) {
                            ok = false;
                            break;
                        }
                    }
                    if (ok) {
                        for (int p : list) {
                            visited[p] = true;
                            ret[p] = id;
                        }
                        id++;
                        remains[t - 2]--;
                        break;
                    } else {
                        int p = list[0];
                        int[] cur = new int[t];
                        int[] bestScore = new int[]{ -1000 * t };
                        int row = p / N;
                        int col = p % N;
                        cur[0] = p;
                        visited[p] = true;
                        dfs(visited, 1, row, col, grid[p], cur, bestScore, tp.item2);
                        visited[p] = false;
                        if (bestScore[0] > -1000 * t) {
                            tp.item1 = bestScore[0];
                            pqs[t].add(tp);
                        }
                    }
                }
            }
        }
        for (int t = 7; t >= 2; t --) {
            PQwithTpIIa pq = pqs[t];
            for (int i = remains[t - 2]; i > 0; i--) {
                while (!pq.isEmpty()) {
                    TpIIa tp = pq.poll();
                    int[] list = tp.item2;
                    if (visited[list[0]]) { continue; }
                    boolean ok = true;
                    for (int p : list) {
                        if (visited[p]) {
                            ok = false;
                            break;
                        }
                    }
                    if (ok) {
                        for (int p : list) {
                            visited[p] = true;
                            ret[p] = id;
                        }
                        id++;
                        remains[t - 2]--;
                        break;
                    } else {
                        int p = list[0];
                        int[] cur = new int[t];
                        int[] bestScore = new int[]{ -1000 * t };
                        int row = p / N;
                        int col = p % N;
                        cur[0] = p;
                        visited[p] = true;
                        dfs(visited, 1, row, col, grid[p], cur, bestScore, tp.item2);
                        visited[p] = false;
                        if (bestScore[0] > -1000 * t) {
                            tp.item1 = bestScore[0];
                            pqs[t].add(tp);
                        }
                    }
                }
            }
        }
    }

    void solveBadGreedy(int[] ret, PQwithTpIIa[] pqs, boolean[] visited) {
        int id = 1;
        int[] remains = Arrays.copyOf(tiles, tiles.length);
        for (;;) {
            TpIIa best = null;
            for (int t = 2; t <= 7; t++) {
                if (remains[t - 2] == 0) { continue; }
                TpIIa tp = pqs[t].peek();
                if (tp == null) { continue; }
                if (best == null
                    || tp.item1 > best.item1
                    || (tp.item1 == best.item1 && tp.item2.length < best.item2.length)
                ) {
                    best = tp;
                }
            }
            if (best == null) {
                break;
            }
            if (best.item1 <= 0) {
                break;
            }
            pqs[best.item2.length].poll();
            if (visited[best.item2[0]]) {
                continue;
            }
            boolean ok = true;
            for (int p : best.item2) {
                if (visited[p]) {
                    ok = false;
                    break;
                }
            }
            if (ok) {
                for (int p : best.item2) {
                    visited[p] = true;
                    ret[p] = id;
                }
                id++;
                remains[best.item2.length - 2]--;
            } else {
                int[] cur = new int[best.item2.length];
                int[] bestScore = new int[]{ -1000 * cur.length };
                int p = best.item2[0];
                cur[0] = p;
                visited[p] = true;
                dfs(visited, 1, p / N, p % N, grid[p], cur, bestScore, best.item2);
                visited[p] = false;
                if (bestScore[0] > -1000 * cur.length) {
                    best.item1 = bestScore[0];
                    pqs[cur.length].add(best);
                }
            }
        }

        for (;;) {
            TpIIa best = null;
            for (int t = 2; t <= 7; t++) {
                if (remains[t - 2] == 0) { continue; }
                TpIIa tp = pqs[t].peek();
                if (tp == null) { continue; }
                if (best == null
                    || tp.item1 > best.item1
                    || (tp.item1 == best.item1 && tp.item2.length > best.item2.length)
                ) {
                    best = tp;
                }
            }
            if (best == null) {
                break;
            }
            pqs[best.item2.length].poll();
            if (visited[best.item2[0]]) {
                continue;
            }
            boolean ok = true;
            for (int p : best.item2) {
                if (visited[p]) {
                    ok = false;
                    break;
                }
            }
            if (ok) {
                for (int p : best.item2) {
                    visited[p] = true;
                    ret[p] = id;
                }
                id++;
                remains[best.item2.length - 2]--;
            } else {
                int[] cur = new int[best.item2.length];
                int[] bestScore = new int[]{ -1000 * cur.length };
                int p = best.item2[0];
                cur[0] = p;
                visited[p] = true;
                dfs(visited, 1, p / N, p % N, grid[p], cur, bestScore, best.item2);
                visited[p] = false;
                if (bestScore[0] > -1000 * cur.length) {
                    best.item1 = bestScore[0];
                    pqs[cur.length].add(best);
                }
            }
        }
    }


    void solveGreedyWithAvg(int[] ret, PQwithTpIIaD[] pqs, boolean[] visited) {
        int id = 1;
        int[] remains = Arrays.copyOf(tiles, tiles.length);
    outerloop:
        for (int t = 7; t >= 2; t --) {
            PQwithTpIIaD pq = pqs[t];
            for (int i = remains[t - 2]; i > 0; i--) {
                while (!pq.isEmpty()) {
                    TpIIaD tp = pq.poll();
                    int[] list = tp.item2;
                    if (visited[list[0]]) { continue; }
                    if (tp.item1 <= 0) {
                        pq.add(tp);
                        continue outerloop;
                    }
                    boolean ok = true;
                    for (int p : list) {
                        if (visited[p]) {
                            ok = false;
                            break;
                        }
                    }
                    if (ok) {
                        for (int p : list) {
                            visited[p] = true;
                            ret[p] = id;
                        }
                        id++;
                        remains[t - 2]--;
                        break;
                    } else {
                        int p = list[0];
                        int[] cur = new int[t];
                        long[][] bestScore = new long[][]{ new long[]{ -1000L * (long)t, 0L, 0L } };
                        int row = p / N;
                        int col = p % N;
                        cur[0] = p;
                        visited[p] = true;
                        dfsWithSum(visited, 1, row, col, (long)grid[p], cur, bestScore, tp.item2);
                        visited[p] = false;
                        if (bestScore[0][0] > -1000L * (long)t) {
                            tp.item1 = (int)bestScore[0][0];
                            tp.item3 = (double)bestScore[0][1] / (double)bestScore[0][2];
                            pqs[t].add(tp);
                        }
                    }
                }
            }
        }
        for (int t = 7; t >= 2; t --) {
            PQwithTpIIaD pq = pqs[t];
            for (int i = remains[t - 2]; i > 0; i--) {
                while (!pq.isEmpty()) {
                    TpIIaD tp = pq.poll();
                    int[] list = tp.item2;
                    if (visited[list[0]]) { continue; }
                    boolean ok = true;
                    for (int p : list) {
                        if (visited[p]) {
                            ok = false;
                            break;
                        }
                    }
                    if (ok) {
                        for (int p : list) {
                            visited[p] = true;
                            ret[p] = id;
                        }
                        id++;
                        remains[t - 2]--;
                        break;
                    } else {
                        int p = list[0];
                        int[] cur = new int[t];
                        long[][] bestScore = new long[][]{ new long[]{ -1000L * (long)t, 0L, 0L } };
                        int row = p / N;
                        int col = p % N;
                        cur[0] = p;
                        visited[p] = true;
                        dfsWithSum(visited, 1, row, col, (long)grid[p], cur, bestScore, tp.item2);
                        visited[p] = false;
                        if (bestScore[0][0] > -1000L * (long)t) {
                            tp.item1 = (int)bestScore[0][0];
                            tp.item3 = (double)bestScore[0][1] / (double)bestScore[0][2];
                            pqs[t].add(tp);
                        }
                    }
                }
            }
        }
    }

    void dfs(boolean[] visited, int depth, int row, int col, int score, int[] cur, int[] bestScores, int[][] bests) {
        if (depth > 1 && score > bestScores[depth]) {
            bestScores[depth] = score;
            System.arraycopy(cur, 0, bests[depth], 0, bests[depth].length);
        }
        if (depth == cur.length) {
            return;
        }
        for (int di = 0; di < 4; di++) {
            int row1 = row + DT[di];
            int col1 = col + DT[di + 1];
            if (row1 < 0 || col1 < 0 || row1 >= N || col1 >= N) { continue; }
            int p = row1 * N + col1;
            if (visited[p]) { continue; }
            visited[p] = true;
            cur[depth] = p;
            dfs(visited, depth + 1, row1, col1, score * grid[p], cur, bestScores, bests);
            visited[p] = false;
        }
    }

    void dfs(boolean[] visited, int depth, int row, int col, int score, int[] cur, int[] bestScore, int[] best) {
        if (depth == cur.length) {
            if (score > bestScore[0]) {
                bestScore[0] = score;
                System.arraycopy(cur, 0, best, 0, best.length);
            }
            return;
        }
        for (int di = 0; di < 4; di++) {
            int row1 = row + DT[di];
            int col1 = col + DT[di + 1];
            if (row1 < 0 || col1 < 0 || row1 >= N || col1 >= N) { continue; }
            int p = row1 * N + col1;
            if (visited[p]) { continue; }
            visited[p] = true;
            cur[depth] = p;
            dfs(visited, depth + 1, row1, col1, score * grid[p], cur, bestScore, best);
            visited[p] = false;
        }
    }

    void dfs(BitFlag visited, int depth, int row, int col, int score, int[] cur, int[] bestScore, int[] best) {
        if (depth == cur.length) {
            if (score > bestScore[0]) {
                bestScore[0] = score;
                System.arraycopy(cur, 0, best, 0, best.length);
            }
            return;
        }
        for (int di = 0; di < 4; di++) {
            int row1 = row + DT[di];
            int col1 = col + DT[di + 1];
            if (row1 < 0 || col1 < 0 || row1 >= N || col1 >= N) { continue; }
            int p = row1 * N + col1;
            if (visited.exists(p)) { continue; }
            visited.set(p);
            cur[depth] = p;
            dfs(visited, depth + 1, row1, col1, score * grid[p], cur, bestScore, best);
            visited.remove(p);
        }
    }


    void dfsWithSum(boolean[] visited, int depth, int row, int col, long score, int[] cur, long[][] bestScores, int[][] bests) {
        if (depth > 1) {
            bestScores[depth][1] += score;
            bestScores[depth][2]++;
            if (score > bestScores[depth][0]) {
                bestScores[depth][0] = score;
                System.arraycopy(cur, 0, bests[depth], 0, bests[depth].length);
            }
        }
        if (depth == cur.length) {
            return;
        }
        for (int di = 0; di < 4; di++) {
            int row1 = row + DT[di];
            int col1 = col + DT[di + 1];
            if (row1 < 0 || col1 < 0 || row1 >= N || col1 >= N) { continue; }
            int p = row1 * N + col1;
            if (visited[p]) { continue; }
            visited[p] = true;
            cur[depth] = p;
            dfsWithSum(visited, depth + 1, row1, col1, score * (long)grid[p], cur, bestScores, bests);
            visited[p] = false;
        }
    }

    void dfsWithSum(boolean[] visited, int depth, int row, int col, long score, int[] cur, long[][] bestScore, int[] best) {
        if (depth == cur.length) {
            bestScore[0][1] += score;
            bestScore[0][2]++;
            if (score > bestScore[0][0]) {
                bestScore[0][0] = score;
                System.arraycopy(cur, 0, best, 0, best.length);
            }
            return;
        }
        for (int di = 0; di < 4; di++) {
            int row1 = row + DT[di];
            int col1 = col + DT[di + 1];
            if (row1 < 0 || col1 < 0 || row1 >= N || col1 >= N) { continue; }
            int p = row1 * N + col1;
            if (visited[p]) { continue; }
            visited[p] = true;
            cur[depth] = p;
            dfsWithSum(visited, depth + 1, row1, col1, score * (long)grid[p], cur, bestScore, best);
            visited[p] = false;
        }
    }


    void reId(int[] ret) {
        int maxId = 0;
        for (int id : ret) {
            maxId = Math.max(maxId, id);
        }
        int[] newId = new int[maxId + 1];
        for (int id : ret) {
            if (id >= 0) {
                newId[id] = -1;
            }
        }
        int id = 1;
        for (int i = 0; i < newId.length; i++) {
            if (newId[i] < 0) {
                newId[i] = id;
                id++;
            }
        }
        for (int i = 0; i < ret.length; i++) {
            if (ret[i] >= 0) {
                ret[i] = newId[ret[i]];
            }
        }
    }

    long reScoreing(int[] ret) {
        boolean[] used = new boolean[N * N + 1];
        int[] scores = new int[N * N + 1];
        Arrays.fill(scores, 1);
        long score = 0;
        for (int i = 0; i < ret.length; i++) {
            if (ret[i] < 0) {
                score -= 1000L;
            } else {
                used[ret[i]] = true;
                scores[ret[i]] *= grid[i];
            }
        }
        for (int i = 0; i < scores.length; i++) {
            if (used[i]) {
                score += scores[i];
            }
        }
        return score;
    }

    private static final boolean[] SPLITABLE = new boolean[1 << 8];
    private static final int POS_UL = 1 << 0;
    private static final int POS_U  = 1 << 1;
    private static final int POS_UR = 1 << 2;
    private static final int POS_R  = 1 << 3;
    private static final int POS_DR = 1 << 4;
    private static final int POS_D  = 1 << 5;
    private static final int POS_DL = 1 << 6;
    private static final int POS_L  = 1 << 7;
    static {
        for (int i = (1 << 8) - 1; i > 0; i--) {
            int c = Integer.bitCount(i);
            int p = 0;
            for (int k = 0; k < 8; k++) {
                int x = (k + 1) & 7;
                if ((i & (1 << k)) == 0 && (i & (1 << x)) != 0) {
                    p = x;
                    break;
                }
            }
            for (int k = 0; k < 8; k++) {
                if ((i & (1 << p)) == 0) {
                    break;
                }
                c--;
                p = (p + 1) & 7;
            }
            SPLITABLE[i] = c == 0;
        }
    }

    boolean canSplitCell(int[] ret, int row, int col) {
        int p = row * N + col;
        int id = ret[p];
        int f = 0;
        if (col > 0 && ret[p - 1] == id) { f |= POS_L; }
        if (col < N - 1 && ret[p + 1] == id) { f |= POS_R; }
        if (row > 0) {
            if (ret[p - N] == id) { f |= POS_U; }
            if (col > 0 && ret[p - N - 1] == id) { f |= POS_UL; }
            if (col < N - 1 && ret[p - N + 1] == id) { f |= POS_UR; }
        }
        if (row < N - 1) {
            if (ret[p + N] == id) { f |= POS_D; }
            if (col > 0 && ret[p + N - 1] == id) { f |= POS_DL; }
            if (col < N - 1 && ret[p + N + 1] == id) { f |= POS_DR; }
        }
        return SPLITABLE[f];
    }

    static final int[] DT = new int[]{ 1, 0, -1, 0, 1 };

    long poorHillClimb(int[] ret) {
        return poorHillClimb(ret, 50L);
    }

    long poorHillClimb(int[] ret, long limitSize) {
        int maxId = 0;
        for (int id : ret) {
            maxId = Math.max(maxId, id);
        }
        int idLimit = N * N + 1;
        int[] idStack = new int[idLimit];
        int stackCnt = 0;
        for (int i = maxId + 1; i < idStack.length; i++) {
            idStack[stackCnt] = i;
            stackCnt++;
        }
        // System.err.println("idLimit: " + idLimit + ", maxId: " + maxId + ", stackCnt: " + stackCnt);

        int[] remains = Arrays.copyOf(tiles, tiles.length);
        int[] sizes = new int[idLimit];
        int[] zeros = new int[idLimit];
        int[] subScores = new int[idLimit];
        Arrays.fill(subScores, 1);
        for (int i = 0; i < ret.length; i++) {
            int id = ret[i];
            if (id < 0) { continue; }
            sizes[id]++;
            int tag = grid[i];
            if (tag == 0) {
                zeros[id]++;
            } else {
                subScores[id] *= tag;
            }
        }

        for (int size : sizes) {
            if (size == 0) {
                continue;
            }
            remains[size - 2]--;
        }

        int[] DTP = new int[]{ N, -1, -N, 1 };

        long time0 = System.currentTimeMillis();
        long time1 = time0;
        long diff = time1 - time0;
        long limit = time0 + limitSize;

        for (int cy = 0; cy < 1000000; cy++) {
            time0 = time1;
            time1 = System.currentTimeMillis();
            diff = Math.max(diff, time1 - time0);
            if (time1 + diff > limit) {
                // System.err.println("limit: " + cy);
                break;
            }

            int changes = 0;

            for (int row = 0; row < N; row++) {
                for (int col = 0; col < N; col++) {
                    int p1 = row * N + col;
                    int id1 = ret[p1];
                    int tag = grid[p1];
                    boolean sp = id1 >= 0 && canSplitCell(ret, row, col);
                    if (!sp && id1 >= 0) { continue; }
                    for (int di = 0; di < 4; di++) {
                        int row2 = row + DT[di];
                        int col2 = col + DT[di + 1];
                        if (row2 < 0 || col2 < 0 || row2 >= N || col2 >= N) { continue; }
                        int p2 = row2 * N + col2;
                        int id2 = ret[p2];
                        if (id1 < 0) {
                            if (id2 < 0) {
                                if (remains[0] == 0) { continue; }
                                stackCnt--;
                                int id = idStack[stackCnt];
                                sizes[id] = 2;
                                remains[0]--;
                                subScores[id] = 1;
                                zeros[id] = 0;
                                if (tag == 0) { zeros[id]++; } else { subScores[id] *= tag; }
                                if (grid[p2] == 0) { zeros[id]++; } else { subScores[id] *= grid[p2]; }
                                ret[p1] = id;
                                ret[p2] = id;
                                changes++;
                                break;
                            }
                            int sz2 = sizes[id2] - 2;
                            if (sz2 == T - 1) { continue; }
                            if (remains[sz2 + 1] == 0) { continue; }
                            if (tag * subScores[id2] + 1000 < subScores[id2]) { continue; }
                            sizes[id2]++;
                            remains[sz2]++;
                            remains[sz2 + 1]--;
                            if (tag == 0) {
                                zeros[id2]++;
                            } else {
                                subScores[id2] *= tag;
                            }
                            ret[p1] = id2;
                            changes++;
                            break;
                        }
                        if (!sp) { continue; }
                        int sz1 = sizes[id1] - 2;
                        if (id1 == id2) {
                            int bef = (zeros[id1] > 0 ? 0 : subScores[id1]);
                            if (sz1 == 0) {
                                if (-2000 < bef) { continue; }
                                sizes[id1] = 0;
                                zeros[id1] = 0;
                                subScores[id1] = 1;
                                idStack[stackCnt] = id1;
                                stackCnt++;
                                ret[p2] = -1;
                            } else if (remains[sz1 - 1] == 0) {
                                continue;
                            } else {
                                int aft = -1000;
                                if (tag != 0) {
                                    aft += bef / tag;
                                } else if (zeros[id1] == 1) {
                                    aft += subScores[id1];
                                }
                                if (aft < bef) { continue; }
                                if (tag != 0) {
                                    subScores[id1] /= tag;
                                } else {
                                    zeros[id1]--;
                                }
                                sizes[id1]--;
                                remains[sz1 - 1]--;
                            }
                            ret[p1] = -1;
                            remains[sz1]++;
                            changes++;
                            break;
                        }
                        if (id2 < 0) {
                            int sc1 = (zeros[id1] > 0 ? 0 : subScores[id1]);
                            if (sz1 == 0) {
                                int sc2 = tag * grid[p2];
                                if (sc2 < sc1) { continue; }
                                int p3 = -1; // IndexOutOfBoundsException
                                for (int i = 0; i < 4; i++) {
                                    p3 = p1 + DTP[i];
                                    if (p3 < 0 || p3 >= ret.length) { continue; }
                                    if (ret[p3] != id1) { continue; }
                                    break;
                                }
                                ret[p2] = id1;
                                ret[p3] = -1;
                                zeros[id1] = 0;
                                subScores[id1] = 1;
                                if (tag == 0) { zeros[id1]++; } else { subScores[id1] *= tag; }
                                if (grid[p2] == 0) { zeros[id1]++; } else { subScores[id1] *= grid[p2]; }
                                changes++;
                                break;
                            }
                            if (remains[0] == 0) { continue; }
                            if (remains[sz1 - 1] == 0) { continue; }
                            if (sz1 - 1 == 0 && remains[0] < 2) { continue; }
                            int bef = sc1 - 1000;
                            int aft = tag * grid[p2];
                            if (tag != 0) {
                                aft += sc1 / tag;
                            } else if (zeros[id1] == 1) {
                                aft += subScores[id1];
                            }
                            if (aft < bef) { continue; }
                            stackCnt--;
                            int id = idStack[stackCnt];
                            sizes[id] = 2;
                            zeros[id] = 0;
                            subScores[id] = 1;
                            if (tag == 0) {
                                zeros[id]++;
                                zeros[id1]--;
                            } else {
                                subScores[id] *= tag;
                                subScores[id1] /= tag;
                            }
                            if (grid[p2] == 0) {
                                zeros[id]++;
                            } else {
                                subScores[id] *= grid[p2];
                            }
                            sizes[id1]--;
                            remains[0]--;
                            remains[sz1]++;
                            remains[sz1 - 1]--;
                            ret[p1] = id;
                            ret[p2] = id;
                            changes++;
                            break;
                        }
                        int sz2 = sizes[id2] - 2;
                        if (sz2 == T - 1) { continue; }
                        int sc1 = (zeros[id1] > 0 ? 0 : subScores[id1]);
                        int sc2 = (zeros[id2] > 0 ? 0 : subScores[id2]);
                        int bef = sc1 + sc2;
                        int aft = sc2 * tag;
                        if (sz1 == 0) {
                            aft -= 1000;
                        } else if (tag != 0) {
                            aft += sc1 / tag;
                        } else if (zeros[id1] == 1) {
                            aft += subScores[id1];
                        }
                        if (aft < bef) { continue; }
                        if (sz1 == 0) {
                            if (remains[sz2 + 1] == 0) { continue; }
                            int p3 = -1; // IndexOutOfBoundsException
                            for (int i = 0; i < 4; i++) {
                                p3 = p1 + DTP[i];
                                if (p3 < 0 || p3 >= ret.length) { continue; }
                                if (ret[p3] != id1) { continue; }
                                break;
                            }
                            sizes[id1] = 0;
                            zeros[id1] = 0;
                            subScores[id1] = 1;
                            idStack[stackCnt] = id1;
                            stackCnt++;
                            sizes[id2]++;
                            if (tag == 0) {
                                zeros[id2]++;
                            } else {
                                subScores[id2] *= tag;
                            }
                            remains[sz1]++;
                            remains[sz2]++;
                            remains[sz2 + 1]--;
                            ret[p1] = id2;
                            ret[p3] = -1;
                            changes++;
                            break;
                        }
                        remains[sz1]++;
                        remains[sz2]++;
                        remains[sz1 - 1]--;
                        remains[sz2 + 1]--;
                        if (remains[sz1 - 1] < 0 || remains[sz2 + 1] < 0) {
                            remains[sz1]--;
                            remains[sz2]--;
                            remains[sz1 - 1]++;
                            remains[sz2 + 1]++;
                            continue;
                        }
                        sizes[id1]--;
                        sizes[id2]++;
                        if (tag == 0) {
                            zeros[id1]--;
                            zeros[id2]++;
                        } else {
                            subScores[id1] /= tag;
                            subScores[id2] *= tag;
                        }
                        ret[p1] = id2;
                        changes++;
                        break;
                    }
                }
            }

            if (changes == 0) {
                // System.err.println("nochanges: " + cy);
                break;
            }
        }

        long score = -1000L * (long)(N * N);
        for (int i = 0; i < subScores.length; i++) {
            if (sizes[i] > 0) {
                if (zeros[i] == 0) {
                    score += (long)subScores[i];
                }
                score += 1000L * (long)sizes[i];
            }
        }
        return score;
    }

    int calcSubScore(int[] ret, int id) {
        int score = 1;
        for (int i = 0; i < ret.length; i++) {
            if (ret[i] == id) {
                score *= grid[i];
            }
        }
        return score;
    }

    static final double INITIAL_TEMPERATURE = 1.5;
    static final double COOL_DOWN = 0.99;
    static final int MINI_CYCLE = 5000;

    boolean accept(long tmpScore, long curScore, double temperature) {
        if (tmpScore < curScore) {
            double s = Math.pow(Math.E, (double)(tmpScore - curScore) / temperature);
            return rand.nextDouble() <= s;
        }
        return true;
    }

    long poorSA(int[] best, long bestScore, long limitSize) {

        int[] ret = Arrays.copyOf(best, best.length);
        long curScore = bestScore;

        int maxId = 0;
        for (int id : ret) {
            maxId = Math.max(maxId, id);
        }
        int idLimit = N * N + 1;
        int[] idStack = new int[idLimit];
        int stackCnt = 0;
        for (int i = maxId + 1; i < idStack.length; i++) {
            idStack[stackCnt] = i;
            stackCnt++;
        }
        // System.err.println("idLimit: " + idLimit + ", maxId: " + maxId + ", stackCnt: " + stackCnt);

        int[] remains = Arrays.copyOf(tiles, tiles.length);
        int[] sizes = new int[idLimit];
        int[] zeros = new int[idLimit];
        int[] subScores = new int[idLimit];
        Arrays.fill(subScores, 1);
        for (int i = 0; i < ret.length; i++) {
            int id = ret[i];
            if (id < 0) { continue; }
            sizes[id]++;
            int tag = grid[i];
            if (tag == 0L) {
                zeros[id]++;
            } else {
                subScores[id] *= tag;
            }
        }

        for (int size : sizes) {
            if (size == 0) {
                continue;
            }
            remains[size - 2]--;
        }

        int[] DTP = new int[]{ N, -1, -N, 1 };

        long time0 = System.currentTimeMillis();
        long time1 = time0;
        long diff = time1 - time0;
        long limit = time0 + limitSize;

        int[] list = new int[N * N];
        for (int i = 0; i < list.length; i++) {
            list[i] = i;
        }
        for (int i = list.length - 1; i > 0; i--) {
            int j = rand.nextInt(i + 1);
            int t = list[i]; list[i] = list[j]; list[j] = t;
        }
        int listp = 0;
        double temperature = INITIAL_TEMPERATURE * Math.log(1.0 + (double)Math.abs(curScore));

        for (int cy = 0; cy < 1_000_000_000; cy++) {
            time0 = time1;
            time1 = System.currentTimeMillis();
            diff = Math.max(diff, time1 - time0);
            if (time1 + diff > limit) {
                System.err.println("limit: " + cy);
                break;
            }
            for (int i = list.length - 1; i > 0; i--) {
                int j = rand.nextInt(i + 1);
                int t = list[i]; list[i] = list[j]; list[j] = t;
            }
            long beforeScore = bestScore;

            for (int mcy = 0; mcy < ret.length * 2; mcy++) {

                int p1 = list[listp];
                listp++;
                if (listp > N) { listp = 0; }
                int row = p1 / N;
                int col = p1 % N;
                int id1 = ret[p1];
                int tag = grid[p1];
                boolean sp = id1 >= 0 && canSplitCell(ret, row, col);
                if (!sp && id1 >= 0) {
                    if (sizes[id1] < 4 || stackCnt < 2 || rand.nextDouble() < 0.7) {
                        continue;
                    }
                    int c = 0;
                    for (int di = 0; di < 4; di++) {
                        int p2 = p1 + DTP[di];
                        if (p2 < 0 || p2 >= ret.length) { continue; }
                        if (ret[p2] != id1) { continue; }
                        c++;
                    }
                    if (c != 2) { continue; }
                    stackCnt--;
                    int id2 = idStack[stackCnt];
                    stackCnt--;
                    int id3 = idStack[stackCnt];
                    ret[p1] = -1;
                    for (int di = 0; di < 4; di++) {
                        int p2 = p1 + DTP[di];
                        if (p2 < 0 || p2 >= ret.length) { continue; }
                        if (ret[p2] != id1) { continue; }
                        int[] cnt = new int[]{ 1, 0, 1 };
                        ret[p2] = id2;
                        if (grid[p2] == 0) {
                            cnt[1]++;
                        } else {
                            cnt[2] *= grid[p2];
                        }
                        fillIdAndCount(DTP, ret, p2, id1, id2, cnt);
                        sizes[id2] = cnt[0];
                        zeros[id2] = cnt[1];
                        subScores[id2] = cnt[2];
                        break;
                    }
                    for (int di = 0; di < 4; di++) {
                        int p3 = p1 + DTP[di];
                        if (p3 < 0 || p3 >= ret.length) { continue; }
                        if (ret[p3] != id1) { continue; }
                        int[] cnt = new int[]{ 1, 0, 1 };
                        ret[p3] = id3;
                        if (grid[p3] == 0) {
                            cnt[1]++;
                        } else {
                            cnt[2] *= grid[p3];
                        }
                        fillIdAndCount(DTP, ret, p3, id1, id3, cnt);
                        sizes[id3] = cnt[0];
                        zeros[id3] = cnt[1];
                        subScores[id3] = cnt[2];
                        break;
                    }
                    if (sizes[id2] < sizes[id3]) {
                        ret[p1] = id2;
                        sizes[id2]++;
                        if (tag == 0) {
                            zeros[id2]++;
                        } else {
                            subScores[id2] *= tag;
                        }
                    } else {
                        ret[p1] = id3;
                        sizes[id3]++;
                        if (tag == 0) {
                            zeros[id3]++;
                        } else {
                            subScores[id3] *= tag;
                        }
                    }
                    int bef = (zeros[id1] > 0 ? 0 : subScores[id1]);
                    int aft = (zeros[id2] > 0 ? 0 : subScores[id2])
                        + (zeros[id3] > 0 ? 0 : subScores[id3]);
                    if (sizes[id2] < 2 || sizes[id3] < 2
                            || sizes[id2] + sizes[id3] != sizes[id1]
                            || remains[sizes[id2]-2] == 0
                            || remains[sizes[id3]-2] == 0
                            || (sizes[id2] == sizes[id3] && remains[sizes[id2]-2] < 2)
                            || aft < bef
                            || !accept(curScore - bef + aft, curScore, temperature)) {
                        for (int di = 0; di < 4; di++) {
                            int p2 = p1 + DTP[di];
                            if (p2 < 0 || p2 >= ret.length) { continue; }
                            if (ret[p2] == id2) {
                                fillId(DTP, ret, p2, id2, id1);
                            } else if (ret[p2] == id3) {
                                fillId(DTP, ret, p2, id3, id1);
                            }
                        }
                        ret[p1] = id1;
                        sizes[id2] = 0;
                        zeros[id2] = 0;
                        subScores[id2] = 1;
                        sizes[id3] = 0;
                        zeros[id3] = 0;
                        subScores[id3] = 1;
                        idStack[stackCnt] = id2;
                        stackCnt++;
                        idStack[stackCnt] = id3;
                        stackCnt++;
                        continue;
                    }
                    remains[sizes[id1] - 2]++;
                    remains[sizes[id2] - 2]--;
                    remains[sizes[id3] - 2]--;
                    sizes[id1] = 0;
                    zeros[id1] = 0;
                    subScores[id1] = 1;
                    idStack[stackCnt] = id1;
                    stackCnt++;
                    curScore -= bef;
                    curScore += aft;
                    if (curScore > bestScore) {
                        bestScore = curScore;
                        System.arraycopy(ret, 0, best, 0, best.length);
                    }
                    continue;
                }
                for (int di = 0; di < 4; di++) {
                    int row2 = row + DT[di];
                    int col2 = col + DT[di + 1];
                    if (row2 < 0 || col2 < 0 || row2 >= N || col2 >= N) { continue; }
                    int p2 = row2 * N + col2;
                    int id2 = ret[p2];
                    if (id1 < 0) {
                        if (id2 < 0) {
                            if (remains[0] == 0) { continue; }
                            stackCnt--;
                            int id = idStack[stackCnt];
                            sizes[id] = 2;
                            remains[0]--;
                            subScores[id] = 1;
                            zeros[id] = 0;
                            if (tag == 0) { zeros[id]++; } else { subScores[id] *= tag; }
                            if (grid[p2] == 0) { zeros[id]++; } else { subScores[id] *= grid[p2]; }
                            ret[p1] = id;
                            ret[p2] = id;
                            curScore += (long)(2000 + tag * grid[p2]);
                            if (curScore > bestScore) {
                                bestScore = curScore;
                                System.arraycopy(ret, 0, best, 0, best.length);
                            }
                            break;
                        }
                        int sz2 = sizes[id2] - 2;
                        if (sz2 == T - 1) { continue; }
                        if (remains[sz2 + 1] == 0) { continue; }
                        if (tag * subScores[id2] + 1000 < subScores[id2]
                            && !accept(curScore + 1000L + (long)(zeros[id2] > 0 ? 0 : subScores[id2])
                                * (long)(tag - 1), curScore, temperature)) { continue; }
                        curScore -= -1000L + (long)(zeros[id2] > 0 ? 0 : subScores[id2]);
                        sizes[id2]++;
                        remains[sz2]++;
                        remains[sz2 + 1]--;
                        if (tag == 0) {
                            zeros[id2]++;
                        } else {
                            subScores[id2] *= tag;
                        }
                        ret[p1] = id2;
                        curScore += (long)(zeros[id2] > 0 ? 0 : subScores[id2]);
                        if (curScore > bestScore) {
                            bestScore = curScore;
                            System.arraycopy(ret, 0, best, 0, best.length);
                        }
                        break;
                    }
                    if (!sp) { continue; }
                    int sz1 = sizes[id1] - 2;
                    if (id1 == id2) {
                        int bef = (zeros[id1] > 0 ? 0 : subScores[id1]);
                        if (sz1 == 0) {
                            if (-2000 < bef && !accept(curScore - bef - 2000, curScore, temperature)) { continue; }
                            sizes[id1] = 0;
                            zeros[id1] = 0;
                            subScores[id1] = 1;
                            idStack[stackCnt] = id1;
                            stackCnt++;
                            ret[p2] = -1;
                            curScore -= bef;
                            curScore += -2000;
                        } else if (remains[sz1 - 1] == 0) {
                            continue;
                        } else {
                            int aft = -1000;
                            if (tag != 0) {
                                aft += bef / tag;
                            } else if (zeros[id1] == 1) {
                                aft += subScores[id1];
                            }
                            if (aft < bef && !accept(curScore - bef + aft, curScore, temperature)) { continue; }
                            if (tag != 0) {
                                subScores[id1] /= tag;
                            } else {
                                zeros[id1]--;
                            }
                            sizes[id1]--;
                            remains[sz1 - 1]--;
                            curScore -= bef;
                            curScore += aft;
                        }
                        ret[p1] = -1;
                        remains[sz1]++;
                        if (curScore > bestScore) {
                            bestScore = curScore;
                            System.arraycopy(ret, 0, best, 0, best.length);
                        }
                        break;
                    }
                    if (id2 < 0) {
                        int sc1 = (zeros[id1] > 0 ? 0 : subScores[id1]);
                        if (sz1 == 0) {
                            int sc2 = tag * grid[p2];
                            if (sc2 < sc1 && !accept(curScore - sc1 + sc2, curScore, temperature)) { continue; }
                            int p3 = -1; // IndexOutOfBoundsException
                            for (int i = 0; i < 4; i++) {
                                p3 = p1 + DTP[i];
                                if (p3 < 0 || p3 >= ret.length) { continue; }
                                if (ret[p3] != id1) { continue; }
                                break;
                            }
                            ret[p2] = id1;
                            ret[p3] = -1;
                            zeros[id1] = 0;
                            subScores[id1] = 1;
                            if (tag == 0) { zeros[id1]++; } else { subScores[id1] *= tag; }
                            if (grid[p2] == 0) { zeros[id1]++; } else { subScores[id1] *= grid[p2]; }
                            curScore -= sc1;
                            curScore += sc2;
                            if (curScore > bestScore) {
                                bestScore = curScore;
                                System.arraycopy(ret, 0, best, 0, best.length);
                            }
                            break;
                        }
                        if (remains[0] == 0) { continue; }
                        if (remains[sz1 - 1] == 0) { continue; }
                        if (sz1 - 1 == 0 && remains[0] < 2) { continue; }
                        int bef = sc1 - 1000;
                        int aft = tag * grid[p2];
                        if (tag != 0) {
                            aft += sc1 / tag;
                        } else if (zeros[id1] == 1) {
                            aft += subScores[id1];
                        }
                        if (aft < bef && !accept(curScore - bef + aft, curScore, temperature)) { continue; }
                        stackCnt--;
                        int id = idStack[stackCnt];
                        sizes[id] = 2;
                        zeros[id] = 0;
                        subScores[id] = 1;
                        if (tag == 0) {
                            zeros[id]++;
                            zeros[id1]--;
                        } else {
                            subScores[id] *= tag;
                            subScores[id1] /= tag;
                        }
                        if (grid[p2] == 0) {
                            zeros[id]++;
                        } else {
                            subScores[id] *= grid[p2];
                        }
                        sizes[id1]--;
                        remains[0]--;
                        remains[sz1]++;
                        remains[sz1 - 1]--;
                        ret[p1] = id;
                        ret[p2] = id;
                        curScore -= bef;
                        curScore += aft;
                        if (curScore > bestScore) {
                            bestScore = curScore;
                            System.arraycopy(ret, 0, best, 0, best.length);
                        }
                        break;
                    }
                    int sz2 = sizes[id2] - 2;
                    if (sz2 == T - 1) { continue; }
                    int sc1 = (zeros[id1] > 0 ? 0 : subScores[id1]);
                    int sc2 = (zeros[id2] > 0 ? 0 : subScores[id2]);
                    int bef = sc1 + sc2;
                    if (sz1 + sz2 + 2 < T && remains[sz1 + sz2 + 2] > 0 && rand.nextDouble() < 0.3) {
                        int aftC = sc1 * sc2;
                        if (aftC > bef || accept(curScore - bef + aftC, curScore, temperature)) {
                            sizes[id2] += sizes[id1];
                            zeros[id2] += zeros[id1];
                            subScores[id2] *= subScores[id1];
                            fillId(DTP, ret, p1, id1, id2);
                            sizes[id1] = 0;
                            zeros[id1] = 0;
                            subScores[id1] = 1;
                            idStack[stackCnt] = id1;
                            stackCnt++;
                            remains[sz1]++;
                            remains[sz2]++;
                            remains[sz1 + sz2 + 2]--;
                            curScore -= bef;
                            curScore += aftC;
                            if (curScore > bestScore) {
                                bestScore = curScore;
                                System.arraycopy(ret, 0, best, 0, best.length);
                            }
                            break;
                        }
                    }
                    int aft = sc2 * tag;
                    if (sz1 == 0) {
                        aft -= 1000;
                    } else if (tag != 0) {
                        aft += sc1 / tag;
                    } else if (zeros[id1] == 1) {
                        aft += subScores[id1];
                    }
                    if (aft < bef && !accept(curScore - bef + aft, curScore, temperature)) { continue; }
                    if (sz1 == 0) {
                        if (remains[sz2 + 1] == 0) { continue; }
                        int p3 = -1; // IndexOutOfBoundsException
                        for (int i = 0; i < 4; i++) {
                            p3 = p1 + DTP[i];
                            if (p3 < 0 || p3 >= ret.length) { continue; }
                            if (ret[p3] != id1) { continue; }
                            break;
                        }
                        sizes[id1] = 0;
                        zeros[id1] = 0;
                        subScores[id1] = 1;
                        idStack[stackCnt] = id1;
                        stackCnt++;
                        sizes[id2]++;
                        if (tag == 0) {
                            zeros[id2]++;
                        } else {
                            subScores[id2] *= tag;
                        }
                        remains[sz1]++;
                        remains[sz2]++;
                        remains[sz2 + 1]--;
                        ret[p1] = id2;
                        ret[p3] = -1;
                        curScore -= bef;
                        curScore += aft;
                        if (curScore > bestScore) {
                            bestScore = curScore;
                            System.arraycopy(ret, 0, best, 0, best.length);
                        }
                        break;
                    }
                    remains[sz1]++;
                    remains[sz2]++;
                    remains[sz1 - 1]--;
                    remains[sz2 + 1]--;
                    if (remains[sz1 - 1] < 0 || remains[sz2 + 1] < 0) {
                        remains[sz1]--;
                        remains[sz2]--;
                        remains[sz1 - 1]++;
                        remains[sz2 + 1]++;
                        continue;
                    }
                    sizes[id1]--;
                    sizes[id2]++;
                    if (tag == 0) {
                        zeros[id1]--;
                        zeros[id2]++;
                    } else {
                        subScores[id1] /= tag;
                        subScores[id2] *= tag;
                    }
                    ret[p1] = id2;
                    curScore -= bef;
                    curScore += aft;
                    if (curScore > bestScore) {
                        bestScore = curScore;
                        System.arraycopy(ret, 0, best, 0, best.length);
                    }
                    break;
                }
            }

            if (bestScore <= beforeScore) {
                temperature *= COOL_DOWN;
            }
        }

        return bestScore;
    }

    void fillId(int[] DTP, int[] ret, int p, int idFrom, int idTo) {
        for (int i = 0; i < 4; i++) {
            int e = p + DTP[i];
            if (e < 0 || e >= ret.length) { continue; }
            if (ret[e] != idFrom) { continue; }
            ret[e] = idTo;
            fillId(DTP, ret, e, idFrom, idTo);
        }
    }

    void fillIdAndCount(int[] DTP, int[] ret, int p, int idFrom, int idTo, int[] cnt) {
        for (int i = 0; i < 4; i++) {
            int e = p + DTP[i];
            if (e < 0 || e >= ret.length) { continue; }
            if (ret[e] != idFrom) { continue; }
            cnt[0]++;
            if (grid[e] == 0) {
                cnt[1]++;
            } else {
                cnt[2] *= grid[e];
            }
            ret[e] = idTo;
            fillIdAndCount(DTP, ret, e, idFrom, idTo, cnt);
        }
    }

    long solveFixedPatterns(int[] ret, long score) {

        int[] pos = new int[N * N];
        int[] num = new int[N * N];

        makeClockwise(pos, num, 0, 0, 1, 0);
        score = fakeDP(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP(ret, score, pos, num);
        makeClockwise(pos, num, N - 1, 0, 0, 1);
        score = fakeDP(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP(ret, score, pos, num);
        makeClockwise(pos, num, N - 1, N - 1, -1, 0);
        score = fakeDP(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP(ret, score, pos, num);
        makeClockwise(pos, num, 0, N - 1, 0, -1);
        score = fakeDP(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP(ret, score, pos, num);

        makeCounterClockwise(pos, num, 0, 0, 0, 1);
        score = fakeDP(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP(ret, score, pos, num);
        makeCounterClockwise(pos, num, 0, N - 1, 1, 0);
        score = fakeDP(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP(ret, score, pos, num);
        makeCounterClockwise(pos, num, N - 1, N - 1, 0, -1);
        score = fakeDP(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP(ret, score, pos, num);
        makeCounterClockwise(pos, num, N - 1, 0, -1, 0);
        score = fakeDP(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP(ret, score, pos, num);

        makeMeanderHorizontal(pos, num, 0, 0, 1, 1);
        score = fakeDP(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP(ret, score, pos, num);
        makeMeanderHorizontal(pos, num, N - 1, 0, -1, 1);
        score = fakeDP(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP(ret, score, pos, num);

        makeMeanderVertical(pos, num, 0, 0, 1, 1);
        score = fakeDP(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP(ret, score, pos, num);
        makeMeanderVertical(pos, num, 0, N - 1, 1, -1);
        score = fakeDP(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP(ret, score, pos, num);

        return score;
    }

    long solveFixedPatterns2(int[] ret, long score) {

        int[] pos = new int[N * N];
        int[] num = new int[N * N];

        makeClockwise(pos, num, 0, 0, 1, 0);
        score = fakeDP2(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP2(ret, score, pos, num);
        makeClockwise(pos, num, N - 1, 0, 0, 1);
        score = fakeDP2(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP2(ret, score, pos, num);
        makeClockwise(pos, num, N - 1, N - 1, -1, 0);
        score = fakeDP2(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP2(ret, score, pos, num);
        makeClockwise(pos, num, 0, N - 1, 0, -1);
        score = fakeDP2(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP2(ret, score, pos, num);

        makeCounterClockwise(pos, num, 0, 0, 0, 1);
        score = fakeDP2(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP2(ret, score, pos, num);
        makeCounterClockwise(pos, num, 0, N - 1, 1, 0);
        score = fakeDP2(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP2(ret, score, pos, num);
        makeCounterClockwise(pos, num, N - 1, N - 1, 0, -1);
        score = fakeDP2(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP2(ret, score, pos, num);
        makeCounterClockwise(pos, num, N - 1, 0, -1, 0);
        score = fakeDP2(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP2(ret, score, pos, num);

        makeMeanderHorizontal(pos, num, 0, 0, 1, 1);
        score = fakeDP2(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP2(ret, score, pos, num);
        makeMeanderHorizontal(pos, num, N - 1, 0, -1, 1);
        score = fakeDP2(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP2(ret, score, pos, num);

        makeMeanderVertical(pos, num, 0, 0, 1, 1);
        score = fakeDP2(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP2(ret, score, pos, num);
        makeMeanderVertical(pos, num, 0, N - 1, 1, -1);
        score = fakeDP2(ret, score, pos, num);
        reverse(pos); reverse(num);
        score = fakeDP2(ret, score, pos, num);

        return score;
    }

    void makeClockwise(int[] pos, int[] num, int x, int y, int dx, int dy) {
        System.err.println("clockwise, x: " + x + ", y: " + y + ", dx: " + dx + ", dy: " + dy);
        boolean[] visited = new boolean[N * N];
        for (int i = 0; i < N * N; i++) {
            int p = y * N + x;
            visited[p] = true;
            pos[i] = p;
            num[i] = grid[p];
            x += dx; y += dy;
            if (x < 0 || y < 0 || x >= N || y >= N || visited[y * N + x]) {
                x -= dx; y -= dy;
                int t = dy; dy = dx; dx = -t;
                x += dx; y += dy;
            }
        }
    }

    void makeCounterClockwise(int[] pos, int[] num, int x, int y, int dx, int dy) {
        System.err.println("counterclockwise, x: " + x + ", y: " + y + ", dx: " + dx + ", dy: " + dy);
        boolean[] visited = new boolean[N * N];
        for (int i = 0; i < N * N; i++) {
            int p = y * N + x;
            visited[p] = true;
            pos[i] = p;
            num[i] = grid[p];
            x += dx; y += dy;
            if (x < 0 || y < 0 || x >= N || y >= N || visited[y * N + x]) {
                x -= dx; y -= dy;
                int t = dy; dy = -dx; dx = t;
                x += dx; y += dy;
            }
        }
    }

    void makeMeanderHorizontal(int[] pos, int[] num, int x, int y, int dx, int dy) {
        System.err.println("meanderhorizontal, x: " + x + ", y: " + y + ", dx: " + dx + ", dy: " + dy);
        boolean[] visited = new boolean[N * N];
        for (int i = 0; i < N * N; i++) {
            int p = y * N + x;
            visited[p] = true;
            pos[i] = p;
            num[i] = grid[p];
            x += dx;
            if (x < 0 || x >= N) {
                x -= dx;
                dx = -dx;
                y += dy;
            }
        }

    }

    void makeMeanderVertical(int[] pos, int[] num, int x, int y, int dx, int dy) {
        System.err.println("meandervertial, x: " + x + ", y: " + y + ", dx: " + dx + ", dy: " + dy);
        boolean[] visited = new boolean[N * N];
        for (int i = 0; i < N * N; i++) {
            int p = y * N + x;
            visited[p] = true;
            pos[i] = p;
            num[i] = grid[p];
            y += dy;
            if (y < 0 || y >= N) {
                y -= dy;
                dy = -dy;
                x += dx;
            }
        }
    }

    void reverse(int[] arr) {
        System.err.println("reveres");
        for (int f = 0, e = arr.length - 1; f < e; f++, e--) {
            int t = arr[f]; arr[f] = arr[e]; arr[e] = t;
        }
    }

    long fakeDP(int[] ret, long preScore, int[] pos, int[] num) {
        long[] sc = new long[N * N + 1];
        int[][] dp = new int[N * N + 1][];
        dp[0] = Arrays.copyOf(tiles, T + 2);
        for (int i = 0; i < N * N; i++) {
            int[] tmpTiles = dp[i];
            long score = sc[i];
            if (i + 1 < dp.length) {
                if (dp[i + 1] == null || score - 1000L > sc[i + 1]) {
                    int[] tmp = Arrays.copyOf(tmpTiles, tmpTiles.length);
                    tmp[T + 1] = i;
                    dp[i + 1] = tmp;
                    sc[i + 1] = score - 1000L;
                }
            }
            for (int t = 0; t < T && i + t + 2 < dp.length; t++) {
                if (tmpTiles[t] == 0) {
                    continue;
                }
                long m = 1L;
                for (int e = 1; e <= t + 2; e++) {
                    m *= (long)num[i + e - 1];
                }
                long tmpScore = score + m;
                if (dp[i + t + 2] == null || tmpScore > sc[i + t + 2]) {
                    int[] tmp = Arrays.copyOf(tmpTiles, tmpTiles.length);
                    tmp[t] -= 1;
                    tmp[T + 1] = i;
                    dp[i + t + 2] = tmp;
                    sc[i + t + 2] = tmpScore;
                }
            }
        }

        int maxP = 0;
        for (int i = 0; i < dp.length; i++) {
            if (sc[i] + (-1000L * (long)(N * N - i)) > sc[maxP] + (-1000L * (long)(N * N - maxP))) {
                maxP = i;
            }
        }
        long bestScore = sc[maxP] + (-1000L * (long)(N * N - maxP));


        int[] tmpRet = new int[N * N];

        Arrays.fill(tmpRet, -1);

        int id = 1;
        while (maxP > 0) {
            int[] tmpTiles = dp[maxP];
            int b = tmpTiles[T + 1];
            if (maxP - b > 1) {
                for (int i = maxP; i > b; i--) {
                    tmpRet[pos[i - 1]] = id;
                }
            }
            id++;
            maxP = b;
        }

        bestScore = poorHillClimb(tmpRet);

        if (bestScore < preScore) {
            return preScore;
        }
        System.err.println("update!");

        System.arraycopy(tmpRet, 0, ret, 0, ret.length);

        return bestScore;
    }

    long fakeDP2(int[] ret, long preScore, int[] pos, int[] num) {
        long[][] sc = new long[N * N + 1][T + 1];
        int[][][] dp = new int[N * N + 1][T + 1][];
        dp[0][0] = Arrays.copyOf(tiles, T + 2);
        for (int i = 0; i < N * N; i++) {
            for (int bt = 0; bt < T + 1; bt++) {
                int[] tmpTiles = dp[i][bt];
                if (tmpTiles == null) { continue; }
                long score = sc[i][bt];
                if (i + 1 < dp.length) {
                    if (dp[i + 1][T] == null || score - 1000L > sc[i + 1][T]) {
                        int[] tmp = Arrays.copyOf(tmpTiles, tmpTiles.length);
                        tmp[T] = bt;
                        tmp[T + 1] = i;
                        dp[i + 1][T] = tmp;
                        sc[i + 1][T] = score - 1000L;
                    }
                }
                for (int t = 0; t < T && i + t + 2 < dp.length; t++) {
                    if (tmpTiles[t] == 0) {
                        continue;
                    }
                    long m = 1L;
                    for (int e = 1; e <= t + 2; e++) {
                        m *= (long)num[i + e - 1];
                    }
                    long tmpScore = score + m;
                    if (dp[i + t + 2][t] == null || tmpScore > sc[i + t + 2][t]) {
                        int[] tmp = Arrays.copyOf(tmpTiles, tmpTiles.length);
                        tmp[t] -= 1;
                        tmp[T] = bt;
                        tmp[T + 1] = i;
                        dp[i + t + 2][t] = tmp;
                        sc[i + t + 2][t] = tmpScore;
                    }
                }
            }
        }

        int maxP = 0, maxT = 0;
        for (int i = 0; i < dp.length; i++) {
            for (int t = 0; t < T + 1; t++) {
                if (sc[i][t] + (-1000L * (long)(N * N - i)) > sc[maxP][maxT] + (-1000L * (long)(N * N - maxP))) {
                    maxP = i;
                    maxT = t;
                }
            }
        }
        long bestScore = sc[maxP][maxT] + (-1000L * (long)(N * N - maxP));


        int[] tmpRet = new int[N * N];

        Arrays.fill(tmpRet, -1);

        int id = 1;
        while (maxP > 0) {
            int[] tmpTiles = dp[maxP][maxT];
            int b = tmpTiles[T + 1];
            if (maxP - b > 1) {
                for (int i = maxP; i > b; i--) {
                    tmpRet[pos[i - 1]] = id;
                }
            }
            id++;
            maxP = b;
            maxT = tmpTiles[T];
        }

        bestScore = poorHillClimb(tmpRet);

        if (bestScore < preScore) {
            return preScore;
        }
        System.err.println("update!");

        System.arraycopy(tmpRet, 0, ret, 0, ret.length);

        return bestScore;
    }


    static final Stopwatch stopwatch = new Stopwatch();

    static int callFindSolution(BufferedReader in, PolyominoCovering polyominoCovering) throws Exception {

        int N = Integer.parseInt(in.readLine());
        int _gridSize = N * N; // Integer.parseInt(in.readLine());
        int[] grid = new int[_gridSize];
        for (int _idx = 0; _idx < _gridSize; _idx++) {
            grid[_idx] = Integer.parseInt(in.readLine());
        }
        int _tilesSize = 6; // Integer.parseInt(in.readLine());
        int[] tiles = new int[_tilesSize];
        for (int _idx = 0; _idx < _tilesSize; _idx++) {
            tiles[_idx] = Integer.parseInt(in.readLine());
        }

        stopwatch.start();

        int[] _result = polyominoCovering.findSolution(N, grid, tiles);

        stopwatch.stop();

        System.err.println("time: " + stopwatch.getTime() + " ms");

        System.out.println(_result.length);
        for (int _it : _result) {
            System.out.println(_it);
        }
        System.out.flush();

        return 0;
    }

    public static void main(String[] args) throws Exception {

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        PolyominoCovering polyominoCovering = new PolyominoCovering();

        // do edit codes if you need

        callFindSolution(in, polyominoCovering);

    }

}

class TpIIa implements Comparable<TpIIa> {
    int item1;
    int[] item2;
    TpIIa(int i1, int[] i2) {
        item1 = i1;
        item2 = i2;
    }
    public int compareTo(TpIIa o) {
        return Integer.compare(o.item1, item1);
    }
}

class TpIIaD implements Comparable<TpIIaD> {
    static int sign = 1;
    int item1;
    int[] item2;
    double item3;
    TpIIaD(int i1, int[] i2, double i3) {
        item1 = i1;
        item2 = i2;
        item3 = i3;
    }
    public int compareTo(TpIIaD o) {
        int cmp = Integer.compare(o.item1, item1);
        if (cmp != 0) {
            return cmp;
        }
        return sign * Double.compare(o.item3, item3);
    }
}

class BitFlag {
    long[] bits;
    private BitFlag() {}
    BitFlag(int n) {
        bits = new long[(n + 63) >> 6];
    }
    BitFlag getCopy() {
        BitFlag cp = new BitFlag();
        cp.bits = Arrays.copyOf(bits, bits.length);
        return cp;
    }
    void set(int i) {
        bits[i >> 6] |= 1L << (i & 63);
    }
    void flip(int i) {
        bits[i >> 6] ^= 1L << (i & 63);
    }
    void remove(int i) {
        bits[i >> 6] ^= get(i);
    }
    long get(int i) {
        return bits[i >> 6] & (1L << (i & 63));
    }
    void setAll(int[] list) {
        for (int i : list) {
            set(i);
        }
    }
    boolean exists(int i) {
        return get(i) != 0L;
    }
    void clear() {
        Arrays.fill(bits, 0L);
    }
    void full() {
        Arrays.fill(bits, -1L);
    }
    void copyTo(BitFlag o) {
        System.arraycopy(bits, 0, o.bits, 0, bits.length);
    }
    boolean all(int[] list) {
        for (int i : list) {
            if (!exists(i)) {
                return false;
            }
        }
        return true;
    }
    boolean any(int[] list) {
        for (int i : list) {
            if (exists(i)) {
                return true;
            }
        }
        return false;
    }
}

class Node<T> {
    Node<T> prev;
    T item;
    Node(Node<T> prev, T item) {
        this.prev = prev;
        this.item = item;
    }
    Node<T> append(T nextItem) {
        return new Node<>(this, nextItem);
    }
}

class State implements Comparable<State> {
    long score;
    int index;
    int[] remains;
    BitFlag flag;
    Node<int[]> node;
    State(long s, int i, int[] r, BitFlag f, Node<int[]> n) {
        score = s;
        index = i;
        remains = r;
        flag = f;
        node = n;
    }
    public int compareTo(State o) {
        return Long.compare(score, o.score);
    }
}

class Stopwatch {
    long time0 = 0;
    long time1= 0;
    long sum = 0;
    void start() {
        time0 = System.currentTimeMillis();
    }
    void stop() {
        time1 = System.currentTimeMillis();
        sum += time1 - time0;
    }
    long getTime() {
        return sum;
    }
    void reset() {
        sum = 0;
    }
}